package id.bootcamp.batch330android.registration

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.bootcamp.batch330android.R

class RegistrationResultActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration_result)
    }
}